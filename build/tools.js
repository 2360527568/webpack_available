const path = require('path')
const glob = require('glob')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const globPath = path.resolve('./src/views/*')

function getPageFolders() {
    let pageFolders = glob.sync(globPath)
    let folders = []
    pageFolders.forEach(element => {
        let arr = element.split('/')
        folders.push({
            folderName: arr[arr.length - 1],
            folderPath: element
        })
    })
    console.log(globPath)
    return folders
}

// console.log('getPageFolders', getPageFolders())

function getEntry() {
    let entrys = {}
    getPageFolders().forEach(element => {
        entrys[element.folderName] = `${element.folderPath}/${element.folderName}.js`
    })
    console.log(entrys)
    return entrys
}

// console.log('getEntry', getEntry())

function getProdHTMLPluginConfig() {
    let configs = []
    getPageFolders().forEach(element => {
        configs.push(
            // 自动绑定js文件并且引用模板文件生成html
            new HtmlWebpackPlugin({
                template: `${element.folderPath}/${element.folderName}.html`,
                filename: element.folderName + '.html', // 输出的hmtl路径
                chunks: [element.folderName],
                minify: {
                    collapseWhitespace: true, // 去除空格换行
                    removeComments: true, // 去除注释
                    removeRedundantAttributes: true, // 删除冗余属性
                    removeScriptTypeAttributes: true, // 去除脚本类型标签
                    removeStyleLinkTypeAttributes: true, // 去除样式类型
                    useShortDoctype: true // 使用短文件形式
                }
                // hash: true
            })
        )
    })
    return configs
}

function getDevHTMLPluginConfig() {
    let configs = []
    getPageFolders().forEach(element => {
        configs.push(
            // 自动绑定js文件并且引用模板文件生成html
            new HtmlWebpackPlugin({
                template: `${element.folderPath}/${element.folderName}.html`,
                filename: element.folderName + '.html', // 输出的hmtl路径
                chunks: [element.folderName]
            })
        )
    })
    return configs
}

// console.log('getHTMLPluginConfig', getHTMLPluginConfig())
module.exports = {
    getEntry,
    getProdHTMLPluginConfig,
    getDevHTMLPluginConfig
}
