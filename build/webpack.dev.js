const tools = require('./tools.js')
const { smart } = require('webpack-merge')
const base = require('./webpack.base')

module.exports = smart(base, {
    mode: 'development',
    devServer: {
        port: 3000,
        progress: true
    },
    devtool: 'source-map',
    plugins: tools.getDevHTMLPluginConfig(),
    module: {
        rules: [
            {
                test: /\.css$/,
                use: [{
                    loader: 'style-loader',
                    options: {
                        insertAt: 'top'
                    }
                }, 'css-loader']
            },
            {
                test: /\.less$/,
                use: [{
                    loader: 'style-loader',
                    options: {
                        insertAt: 'top'
                    }
                }, 'css-loader', 'less-loader']
            }
        ]
    }
})