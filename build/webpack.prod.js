const { smart } = require('webpack-merge')
const base = require('./webpack.base')
const MiniCssExtractCss = require('mini-css-extract-plugin')
const UglifyjsWebpackPlugin = require('uglifyjs-webpack-plugin')
const OptimizeCssAssetsWebpackPlugin = require('optimize-css-assets-webpack-plugin')
const tools = require('./tools.js')

const prodHTMLPluginConfig = tools.getProdHTMLPluginConfig()

const plugins = [
    new MiniCssExtractCss({
        filename: '[name].css'
    })
]

module.exports = smart(base, {
    mode: 'production',
    optimization: { // 优化项
        splitChunks: {
            cacheGroups: {
                common: {
                    chunks: 'initial',
                    minSize: 0,
                    minChunks: 2
                }
            }
        },
        minimizer: [
            new UglifyjsWebpackPlugin({
                cache: true,
                parallel: true,
                sourceMap: true
            }),
            new OptimizeCssAssetsWebpackPlugin()
        ]
    },
    plugins: plugins.concat(prodHTMLPluginConfig),
    module: {
        rules: [
            {
                test: /\.css$/,
                use: [MiniCssExtractCss.loader, 'css-loader']
            },
            {
                test: /\.less$/,
                use: [MiniCssExtractCss.loader, 'css-loader', 'less-loader']
            }
        ]
    }
})