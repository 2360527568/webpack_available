const path = require('path')
const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer')
const StatsWriterPlugin = require("webpack-stats-plugin").StatsWriterPlugin;
const tools = require('./tools.js')

module.exports = {
    entry: tools.getEntry(),
    output: {
        filename: '[name].bundle.js',
        path: path.resolve(__dirname, '../dist'),
        // cdn地址 (全局)
        // publicPath: 'http://127.0.0.1:3000'
    },
    plugins: [
        new BundleAnalyzerPlugin({ analyzerPort: 8919 }),
        new StatsWriterPlugin({
            filename: "stats.json" // Default
        })
    ],
    externals: {
        jquery: 'jQuery'
    },
    module: {
        rules: [{
                test: /\.html$/,
                use: 'html-withimg-loader'
            },
            {
                test: /\.(png|jpg|gif)$/,
                // 当图片小于多少k的时候转化为 base64
                // 否则用 file-loader 直接产出
                use: {
                    loader: 'url-loader',
                    options: {
                        limit: 1 * 1024,
                        // cdn地址 (局部)
                        // publicPath: 'http://cdn.example.com',
                        // outputPath: '/img/'
                    }
                }
            },
            {
                test: /\.js$/,
                use: {
                    loader: 'babel-loader',
                    options: { // 用babel-loader 需要把es6-es5
                        presets: [
                            '@babel/preset-env'
                        ],
                        plugins: [
                            ['@babel/plugin-proposal-decorators', {
                                'legacy': true
                            }],
                            ['@babel/plugin-proposal-class-properties', {
                                'loose': true
                            }]
                        ]
                    }
                }
            }
        ]
    }
}